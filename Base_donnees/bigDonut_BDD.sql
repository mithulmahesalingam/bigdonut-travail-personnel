-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 30 Juillet 2017 à 13:27
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `magasin_pro`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `idArticles` int(11) NOT NULL AUTO_INCREMENT,
  `CategorieArticles` varchar(20) NOT NULL,
  `DescriptionArticles` varchar(80) NOT NULL,
  `PrixunitaireArticles` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idArticles`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--


--
-- Structure de la table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `idClients` int(11) NOT NULL AUTO_INCREMENT,
  `NomClients` varchar(20) NOT NULL,
  `PrenomClients` varchar(20) NOT NULL,
  `AdresseClients` varchar(80) NOT NULL,
  `CodepostalClients` varchar(4) NOT NULL,
  `LocaliteClients` varchar(20) NOT NULL,
  `DatedenaissanceClients` date NOT NULL,
  `CourrielClients` varchar(40) NOT NULL,
  `MotdepasseClients` varchar(60) NOT NULL,
  `FixeClients` varchar(20) NOT NULL,
  `PortableClients` varchar(20) NOT NULL,
  PRIMARY KEY (`idClients`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;



--
-- Structure de la table `commandes`
--

CREATE TABLE IF NOT EXISTS `commandes` (
  `idCommandes` int(11) NOT NULL AUTO_INCREMENT,
  `clients_idClients` int(4) NOT NULL,
  PRIMARY KEY (`idCommandes`),
  KEY `Clients_idClients` (`Clients_idClients`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `commandes`
--


-- --------------------------------------------------------

--
-- Structure de la table `commandes_has_articles`
--

CREATE TABLE IF NOT EXISTS `commandes_has_articles` (
  `commandes_idCommandes` int(11) NOT NULL,
  `articles_idArticles` int(11) NOT NULL,
  `quantitearticleCommandes_has_articles` int(11) NOT NULL,
  PRIMARY KEY (`commandes_idCommandes`,`articles_idArticles`),
  KEY `fk_commandes_has_articles1_articles1_idx` (`articles_idArticles`),
  KEY `fk_commandes_has_articles1_commandes1_idx` (`commandes_idCommandes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`Clients_idClients`) REFERENCES `clients` (`idClients`);

--
-- Contraintes pour la table `commandes_has_articles`
--
ALTER TABLE `commandes_has_articles`
  ADD CONSTRAINT `fk_commandes_has_articles1_commandes1` FOREIGN KEY (`commandes_idCommandes`) REFERENCES `commandes` (`idCommandes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_commandes_has_articles1_articles1` FOREIGN KEY (`articles_idArticles`) REFERENCES `articles` (`idArticles`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
