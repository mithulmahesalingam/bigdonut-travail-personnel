<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>BigDonut</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" href="images/Big_Donut.png">

  </head>
  <body>

<!-- HEADER -->
<header>
  <div class = "header">
      <a  href="welcome.php"><img src="images/logo.png" alt="logo du site" /></a>
  </div>
</header>
<!-- HEADER -->

<!-- NAVIGATION -->

<div class ="nav">
    <nav>
      <ul>
        <li><a href="welcome.php">Accueil</a></li>
        <li><a href="specialites.php">Spécialités</a></li>
        <li><a href="connexionClient.html">Connexion</a></li>
        <li><a href="contact.html">Contact</a></li>
      </ul>
    </nav>
</div>

<!-- NAVIGATION -->

<!-- SECTION ARTICLE -->
<div class="sectionConnexion">

  <section>
    <?php
    $hostname = "hhva.myd.infomaniak.com";
    $servername = "hhva_mithulmhsln";
    $username = "hhva_mithulmhsln";
    $password3 ="JLGUxaypXo";


  	$id = $_SESSION['idClients'];

  	try {
  		$bdd = new PDO("mysql:host=$hostname;dbname=$servername", "$username", "$password3");  // Connexion à la base de données coursphp

  		$bdd->query("SET NAMES 'utf8'");

  		$reponse = $bdd->query("SELECT * FROM clients WHERE idClients = '$id'"); // Requête SQL
  		$donnees = $reponse->fetch();

  		echo "<h2>Modifier vos données personnelles</h2>";
  		echo "<br />";
  		echo "<em>Tous les champs sont obligatoires.</em>";
  		echo "<br />";
  		echo "<br />";
  		echo "<em>Si vous changez d'adresse courriel, nous vous prions de demander une suppression de votre compte auprès de notre webmaster. Vous pourrez ensuite vous réinscrire.</em>";
      echo "<br />";
  		echo "<br />";
  		?>

  		<form method="post" action="EnregistrementModification.php">

  		<label for ="nom" >Votre nom : </label>

  		<input type="text" name="nom" id="nom" value="<?php echo $donnees['NomClients']; ?>" required />

  		<br/>
      <br/>

  		<label for ="prenom">Votre prénom : </label>
  		<input type="text" name ="prenom" id="prenom" value="<?php echo $donnees['PrenomClients']; ?>"  required />

  		<br/>
      <br/>

  		<label for ="adresse">Votre adresse : </label>
  		<input type="text" name ="adresse" id="adresse" value="<?php echo $donnees['AdresseClients']; ?>"  required />

  		<br/>
      <br/>

  		<label for ="codePostal">Votre code postal : </label>
  		<input type="text" name ="codePostal" id="codePostal" value="<?php echo $donnees['CodepostalClients']; ?>"  required />

  		<br/>
      <br/>

  		<label for ="localite">Votre localité : </label>
  		<input type="text" name ="localite" id="localite" value="<?php echo $donnees['LocaliteClients']; ?>"  required />

  		<br/>
      <br/>

  		<?php
  			$dates = explode("-", $donnees['DatedenaissanceClients']);
  			$date = $dates[2] . "/" . $dates[1] . "/" . $dates[0];

  		?>
  		<label for ="dateNaissance">Votre date de naissance : </label>
  		<input type="text" name ="dateNaissance" id="dateNaissance" value="<?php echo $date; ?>"  required />

  		<br/>
      <br/>

  		<label for ="email">Votre adresse courriel : </label>
  		<input type="email" name ="email" id="email" value="<?php echo $donnees['CourrielClients']; ?>"  disabled="true" required />

  		<br/>
      <br/>

  		<label for ="password">Choisissez votre mot de passe : </label>
  		<input type="password" name ="password" id="password" value="<?php echo $donnees['MotdepasseClients']; ?>"  required />

  		<br/>
      <br/>

  		<label for ="password2">Confirmez votre mot de passe : </label>
  		<input type="password" name ="password2" id="password2" value="<?php echo $donnees['MotdepasseClients']; ?>"  required />

  		<br/>
      <br/>

  		<label for ="telFixe">Votre numéro de téléphone fixe : </label>
  		<input type="text" name ="telFixe" id="telFixe" value="<?php echo $donnees['FixeClients']; ?>"  required placeholder="+41 22 757 08 03" />

  		<br/>
      <br/>

  		<label for ="telPortable">Votre numéro de téléphone portable : </label>
  		<input type="text" name ="telPortable" id="telPortable" value="<?php echo $donnees['PortableClients']; ?>"  required placeholder="+41 79 306 39 62" />

  		<br/>
  		<br/>

  		<input type="submit" value="Valider" />
  		<input type="reset" value="Annuler" />

  		<br />
  		<br/>

  		<a href="entree.php">Retour à votre espace membre...</a>
  	</form>


  		<?php
  		$bdd = null;
  	}
  	catch (PDOException $e) {
  		echo "Erreur !: " . $e->getMessage() . "<br />";
  		die();
  	}
  ?>
  </section>
</div>
<!-- SECTION ARTICLE -->

<!-- FOOTER -->
<div class="footer">
  <footer>
    <p>2018 &copy BigDonut, Mithul MAHESALINGAM, tous droits réservés.</p>
  </footer>
</div>
<!-- FOOTER -->

  </body>

</html>
