<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>BigDonut</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" href="images/Big_Donut.png">

  </head>
  <body>

<!-- HEADER -->
<header>
  <div class = "header">
      <a  href="welcome.php"><img src="images/logo.png" alt="logo du site" /></a>
  </div>
</header>
<!-- HEADER -->

<!-- NAVIGATION -->

<div class ="nav">
    <nav>
      <ul>
        <li><a href="welcome.php">Accueil</a></li>
        <li><a href="specialites.php">Spécialités</a></li>
        <li><a href="connexionClient.html">Connexion</a></li>
        <li><a href="contact.html">Contact</a></li>
      </ul>
    </nav>
</div>


<!-- SECTION ARTICLE -->
<div class="sectionConnexion">

  <section>
    <article>
      <p><h1>BIENVENUE !</h1>
      <strong>Big Donut</strong> est un restaurant spécialisé dans la vente de beignets et de boissons.<br/>
    Nous vous proposons un large choix de produits de qualité à un prix raisonnable.<br/><br/>
  <img src="images/donut.jpg"/></p>
    </article>
  </section>
</div>
<!-- SECTION ARTICLE -->

<!-- FOOTER -->
<div class="footer">
  <footer>
    <p>2018 &copy BigDonut, Mithul MAHESALINGAM, tous droits réservés.</p>
  </footer>
</div>
<!-- FOOTER -->

  </body>
</html>
