<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>BigDonut</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" href="images/Big_Donut.png">

  </head>
  <body>

<!-- HEADER -->
<header>
  <div class = "header">
      <a  href="welcome.php"><img src="images/logo.png" alt="logo du site" /></a>
  </div>
</header>
<!-- HEADER -->

<!-- NAVIGATION -->

<div class ="nav">
    <nav>
      <ul>
        <li><a href="welcome.php">Accueil</a></li>
        <li><a href="specialites.php">Spécialités</a></li>
        <li><a href="connexionClient.html">Connexion</a></li>
        <li><a href="contact.html">Contact</a></li>
      </ul>
    </nav>
</div>

<!-- NAVIGATION -->

<!-- SECTION ARTICLE -->
<div class="sectionConnexion">

  <section>
    <?php
    $hostname = "hhva.myd.infomaniak.com";
    $servername = "hhva_mithulmhsln";
    $username = "hhva_mithulmhsln";
    $password3 ="JLGUxaypXo";

  		$_SESSION['panier']=array();
  		$_SESSION['panier']['idArticle'] = array();
  		$_SESSION['panier']['qArticle'] = array();

  		echo "<h2>Vous avez commandé :</h2>";

  		$j = 0;
  		$somme = 0;
  		for ($i = 1; $i <= 10; $i++) {
  			$idArtNum = "ArtNum" . $i;
  			$qArtNum = "qArtNum" . $i;

  			if (isset($_POST[$idArtNum])) {


  				$idArticle = $_POST[$idArtNum];
  				$qArticle = $_POST[$qArtNum];

  				$_SESSION['panier']['idArticle'][$j] = $idArticle;
  				$_SESSION['panier']['qArticle'][$j] = $qArticle;

  				try {
  				$bdd = new PDO("mysql:host=$hostname;dbname=$servername", "$username", "$password3");  // Connexion à la base de données coursphp

  					$bdd->query("SET NAMES 'utf8'");

  					$reponse = $bdd->query("SELECT * FROM articles WHERE idArticles = '$idArticle'"); // Requête SQL
  					$donnees = $reponse->fetch();

  					$somme = $somme + $donnees['PrixunitaireArticles'] * $qArticle;

  					echo $donnees['DescriptionArticles'] . ", " . $donnees['PrixunitaireArticles'] . ", " . " en " . $qArticle . " exemplaire-s" . "<br />";



  					$bdd = NULL; // Déconnexion de MySQL
  				}

  				catch (PDOException $e) {
  					echo "Erreur !: " . $e->getMessage() . "<br />";
  					die();
  				}

  				$j = $j + 1;
  			}
  		}
  		echo "<br />";
  		echo "Pour un montant total de : " . number_format($somme, 2) . " CHF.";
  		echo "<br />";
  		echo "<br />";

  		?>

  		<form method="POST" action="EnregistrerCommande.php">
  		<input type="hidden" name="nArticles" value="<?php echo $j; ?>" />
  		<input type='submit' value='Enregistrer' />
  		<br />
  		</form>

  	<br />
  	<a href="PasserUneCommande.php">Retour à la page présentant les articles pouvant être commandés...</a>
  	<br />
  	<a href="entree.php">Retour à la page d'accueil...</a>
  </section>
</div>
<!-- SECTION ARTICLE -->

<!-- FOOTER -->
<div class="footer">
  <footer>
    <p>2018 &copy BigDonut, Mithul MAHESALINGAM, tous droits réservés.</p>
  </footer>
</div>
<!-- FOOTER -->

  </body>

</html>
