<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>BigDonut</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" href="images/Big_Donut.png">

  </head>
  <body>

<!-- HEADER -->
<header>
  <div class = "header">
      <a  href="welcome.php"><img src="images/logo.png" alt="logo du site" /></a>
  </div>
</header>
<!-- HEADER -->

<!-- NAVIGATION -->

<div class ="nav">
    <nav>
      <ul>
        <li><a href="welcome.php">Accueil</a></li>
        <li><a href="specialites.php">Spécialités</a></li>
        <li><a href="connexionClient.html">Connexion</a></li>
        <li><a href="contact.html">Contact</a></li>
      </ul>
    </nav>
</div>

<!-- NAVIGATION -->

<!-- SECTION ARTICLE -->
<div class="sectionConnexion">

  <section>
    <?php
    $hostname = "hhva.myd.infomaniak.com";
    $servername = "hhva_mithulmhsln";
    $username = "hhva_mithulmhsln";
    $password3 ="JLGUxaypXo";


	try {
			$bdd = new PDO("mysql:host=$hostname;dbname=$servername", "$username", "$password3");   // Connexion à la base de données coursphp

			$bdd->query("SET NAMES 'utf8'");

      $reponse2 = $bdd->query("SELECT * FROM highlight"); // Requête SQL

      echo "<form action='CommandeEffectuee.php' method='post'>";
      echo "<table>";

      while ($donnees2 = $reponse2->fetch()) {
        $id = $donnees2['idHigh'];

        echo "<tr>";
        echo "<td><input type='checkbox' name='ArtNum" . $id ."' value='$id' /></td>";
        echo "<td>" . $donnees2['descriptionHigh'] . "</td>";
        echo "<td>" . $donnees2['prixUnitaireHigh'] . " CHF l'unité</td>";
        echo "<td><input type='number' name='qArtNum" . $id . "' min='1' max='10' step='1' value='1'/>";
        echo " exemplaire-s</td>";
        echo "</tr>";
      }

			$reponse = $bdd->query("SELECT * FROM articles"); // Requête SQL

			echo "<form action='CommandeEffectuee.php' method='post'>";
			echo "<table>";

			while ($donnees = $reponse->fetch()) {
				$id = $donnees['idArticles'];

				echo "<tr>";
				echo "<td><input type='checkbox' name='ArtNum" . $id ."' value='$id' /></td>";
				echo "<td>" . $donnees['DescriptionArticles'] . "</td>";
				echo "<td>" . $donnees['PrixunitaireArticles'] . " CHF l'unité</td>";
				echo "<td><input type='number' name='qArtNum" . $id . "' min='1' max='10' step='1' value='1'/>";
				echo " exemplaire-s</td>";
				echo "</tr>";
			}



			echo "</table>";
			echo "<br />";
			echo "<p class='contact'>Pour un nombre d'exemplaires supérieur à 10, veuillez nous contacter.</p>";
			echo "<br />";
			echo "<br />";
			echo "<input type='reset' value='Recommencer' />";
			echo "<input type='submit' value='Commander' />";
			echo "</form>";

			echo "<br />";

			$bdd = NULL; // Déconnexion de MySQL
		}
		catch (PDOException $e) {
			echo "Erreur !: " . $e->getMessage() . "<br />";
			die();
		}


	?>

  <br />

  <a href="entree.php">Retour à votre espace membre...</a>
  </section>
</div>
<!-- SECTION ARTICLE -->

<!-- FOOTER -->
<div class="footer">
  <footer>
    <p>2018 &copy BigDonut, Mithul MAHESALINGAM, tous droits réservés.</p>
  </footer>
</div>
<!-- FOOTER -->

  </body>

</html>
