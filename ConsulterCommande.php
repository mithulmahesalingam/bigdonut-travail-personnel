<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>BigDonut</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" href="images/Big_Donut.png">

  </head>
  <body>

<!-- HEADER -->
<header>
  <div class = "header">
      <a  href="welcome.php"><img src="images/logo.png" alt="logo du site" /></a>
  </div>
</header>
<!-- HEADER -->

<!-- NAVIGATION -->

<div class ="nav">
    <nav>
      <ul>
        <li><a href="welcome.php">Accueil</a></li>
        <li><a href="specialites.php">Spécialités</a></li>
        <li><a href="connexionClient.html">Connexion</a></li>
        <li><a href="contact.html">Contact</a></li>
      </ul>
    </nav>
</div>

<!-- NAVIGATION -->

<!-- SECTION ARTICLE -->
<div class="sectionConnexion">

  <section>
    <?php
    $hostname = "hhva.myd.infomaniak.com";
    $servername = "hhva_mithulmhsln";
    $username = "hhva_mithulmhsln";
    $password3 ="JLGUxaypXo";


	$id = $_SESSION['idClients'];


	try {
			$bdd = new PDO("mysql:host=$hostname;dbname=$servername", "$username", "$password3");  // Connexion à la base de données coursphp

			$bdd->query("SET NAMES 'utf8'");

			$reponse = $bdd->query("SELECT * FROM clients WHERE idClients = '$id'"); // Requête SQL
			$donnees = $reponse->fetch();

			echo $donnees['NomClients']	. " " . $donnees['PrenomClients'];
			echo "<br />";
			echo $donnees['AdresseClients'];
			echo "<br />";
			echo $donnees['CodepostalClients'] . " " . $donnees['LocaliteClients'];
			echo "<br />";
			echo "<br />";

			echo "<hr />";
			echo "<h3>Récapitulatif des commandes</h3>";
			echo "<hr />";

			$reponseDeCommandes = $bdd->query("SELECT * FROM commandes WHERE Clients_idClients = '$id'");
			$reponseDeCommandes->setFetchMode(PDO::FETCH_BOTH);

			$cpt = 0;

			while ($donneesDeCommandes = $reponseDeCommandes->fetch()) {
				$idCommandes = $donneesDeCommandes['idCommandes'];

				$reponseDeCommHasArt = $bdd->query("SELECT * FROM commandes_has_articles WHERE Commandes_idCommandes = '$idCommandes'");
				$reponseDeCommHasArt->setFetchMode(PDO::FETCH_BOTH);

				$somme = 0;
				$cpt = $cpt +1;

				echo "<p class='enGras'>Commande n° " . $cpt . "</p>";


				while ($donneesDeCommHasArt = $reponseDeCommHasArt->fetch()) {
					$idArticles = $donneesDeCommHasArt['articles_idArticles'];

					$reponseDeArticles = $bdd->query("SELECT * FROM articles WHERE idArticles = '$idArticles'");
					$reponseDeArticles->setFetchMode(PDO::FETCH_BOTH);

					while ($donneesDeArticles = $reponseDeArticles->fetch()) {

						echo $donneesDeArticles['CategorieArticles'] . ", ";
						echo $donneesDeArticles['DescriptionArticles'] . ", ";
						echo $donneesDeArticles['PrixunitaireArticles'] . " CHF l'unité, ";
						echo $donneesDeCommHasArt['quantitearticleCommandes_has_articles'] . " unité-s<br/>";

						$somme = $somme + $donneesDeArticles['PrixunitaireArticles'] * $donneesDeCommHasArt['quantitearticleCommandes_has_articles'];
					}
				}

				echo "<br />";

				echo "Pour un montant total de : " . number_format($somme, 2) . " CHF.";

				echo "<hr />";
			}

			$bdd = NULL; // Déconnexion de MySQL
		}
		catch (PDOException $e) {
			echo "Erreur !: " . $e->getMessage() . "<br />";
			die();
		}

	?>
  </section>
    <a href="entree.php">Retour à votre espace membre...</a>
</div>
<!-- SECTION ARTICLE -->

<!-- FOOTER -->
<div class="footer">
  <footer>
    <p>2018 &copy BigDonut, Mithul MAHESALINGAM, tous droits réservés.</p>
  </footer>
</div>
<!-- FOOTER -->

  </body>

</html>
